export const SETUP_ALREADY_COMPLETE = 'Setup already complete';
export const PLEASE_RUN_SETUP = 'Please run setup';
export const SOMETHING_WENT_WRONG = 'Something went wrong';
export const NOT_CONNECTED = 'not connected';
export const SERVICE_ALREADY_REGISTERED = 'Service already registered';
