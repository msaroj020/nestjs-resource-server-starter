import { Module } from '@nestjs/common';
import { NoteService } from './services/note/note.service';
import { NoteSampleService } from './entities/note-sample/note-sample.service';
import { NoteController } from './controllers/note/note.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NoteEntity } from './entities/note.entity';

@Module({
  imports: [TypeOrmModule.forFeature([NoteEntity])],
  controllers: [NoteController],
  providers: [NoteService, NoteSampleService],
  exports: [NoteService, NoteSampleService],
})
export class NoteModule {}
