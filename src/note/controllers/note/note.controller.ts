import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { NoteDto } from '../../entities/note.dto';
// import { NoteSampleService } from 'src/note/entities/note-sample/note-sample.service';
import { NoteService } from '../../services/note/note.service';
import { UpdateDTO } from '../../entities/update-dto.dto';

@Controller('note')
export class NoteController {
  constructor(private readonly noteService: NoteService) {}
  @Post('create')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  createNote(@Body() note: NoteDto) {
    return this.noteService.createNote(note);
  }

  @Post('delete/:uuid')
  deleteNote(@Param() id) {
    return this.noteService.deleteNote(id);
  }

  @Post('update')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  updateNote(@Body() updateNote: UpdateDTO) {
    return this.noteService.updateNote(updateNote);
  }

  @Get('read')
  read() {
    return this.noteService.listNotes();
  }
}
