import { IsNotEmpty, IsOptional } from 'class-validator';

export class UpdateDTO {
  @IsNotEmpty()
  uuid: string;

  @IsOptional()
  title: string;

  @IsOptional()
  message: string;
}
