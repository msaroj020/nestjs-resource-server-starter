import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { NoteEntity } from '../note.entity';

import { Repository } from 'typeorm';
import { NoteDto } from '../note.dto';
import { UpdateDTO } from '../update-dto.dto';

@Injectable()
export class NoteSampleService {
  constructor(
    @InjectRepository(NoteEntity)
    private readonly noteRepository: Repository<NoteEntity>,
  ) {}

  async create(note: NoteDto) {
    const noteObject = new NoteEntity();
    Object.assign(noteObject, note);
    return await noteObject.save();
  }

  async read() {
    return await this.noteRepository.find();
  }

  async delete(uuid: string) {
    const foundNote = await this.noteRepository.findOne(uuid);
    return await foundNote.remove();
  }

  async update(updateNote: UpdateDTO) {
    const foundNote = await this.noteRepository.findOne({
      uuid: updateNote.uuid,
    });
    if (!foundNote) {
      throw new BadRequestException('note not found');
    }
    Object.assign(foundNote, updateNote);
    return await foundNote.save();
  }
}
