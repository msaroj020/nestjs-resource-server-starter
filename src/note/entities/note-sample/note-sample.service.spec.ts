import { Test, TestingModule } from '@nestjs/testing';
import { NoteSampleService } from './note-sample.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { NoteEntity } from '../note.entity';

describe('NoteSampleService', () => {
  let service: NoteSampleService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        NoteSampleService,
        {
          provide: getRepositoryToken(NoteEntity),
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<NoteSampleService>(NoteSampleService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
