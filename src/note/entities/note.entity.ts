import { BaseEntity, Entity, ObjectIdColumn, ObjectID, Column } from 'typeorm';

@Entity()
export class NoteEntity extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  title: string;

  @Column()
  message: string;
}
