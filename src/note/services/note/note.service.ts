import { Injectable } from '@nestjs/common';
import { NoteDto } from '../../entities/note.dto';
import * as uuidv4 from 'uuid/v4';
import { NoteSampleService } from '../../entities/note-sample/note-sample.service';
import { UpdateDTO } from '../../entities/update-dto.dto';
@Injectable()
export class NoteService {
  constructor(private readonly noteSampleService: NoteSampleService) {}

  createNote(note: NoteDto) {
    note.uuid = uuidv4();
    return this.noteSampleService.create(note);
  }

  listNotes() {
    return this.noteSampleService.read();
  }

  deleteNote(uuid: string) {
    return this.noteSampleService.delete(uuid);
  }

  updateNote(updateNote: UpdateDTO) {
    return this.noteSampleService.update(updateNote);
  }
}
