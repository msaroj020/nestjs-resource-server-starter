import { Test, TestingModule } from '@nestjs/testing';
import { NoteService } from './note.service';
import { NoteSampleService } from '../../entities/note-sample/note-sample.service';

describe('NoteService', () => {
  let service: NoteService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        NoteService,
        {
          provide: NoteSampleService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<NoteService>(NoteService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
